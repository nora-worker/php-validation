<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.0.0
 */
namespace Nora\Module\Validation\Rule;
use Nora\Core\Options\OptionsAccess;

use function Nora\__;

class Int extends Base
{
    protected function initRuleImpl( )
    {
        $this->initOptions([
            'message' => __('数値で入力してください')
        ]);
    }

    protected function validateImpl($value)
    {
        return is_int($value);
    }
}
