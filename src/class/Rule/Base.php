<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.0.0
 */
namespace Nora\Module\Validation\Rule;
use Nora\Core\Options\OptionsAccess;

use function Nora\__;

abstract class Base
{
    use OptionsAccess;

    public function __construct ( )
    {
        $this->initRuleImpl();
    }

    protected function initRuleImpl( )
    {
        $this->initOptions([
            'message' => __('Null以外のデータを入力してください'),
            'errMessage' => __('{{input}}は不正です')
        ]);
    }

    public function getMessage( )
    {
        return $this->formatMessage(
            $this->getOption('message')
        );
    }

    protected function formatMessage($msg)
    {
        return preg_replace_callback('/\{\{(.+?)\}\}/', function ($m) {
            return $this->getOption($m[1]);
        },$msg);
    }

    public function validate($value)
    {
        return $this->validateImpl($value);
    }

    abstract protected function validateImpl($value);

    static public function build($spec)
    {
        $class = get_called_class();
        $validator = new $class();
        $validator->setOption($spec[0]);
        return $validator;
    }
}
