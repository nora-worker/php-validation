<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.0.0
 */
namespace Nora\Module\Validation\Rule;
use Nora\Core\Options\OptionsAccess;

use function Nora\__;

class True extends Base
{
    protected function initRuleImpl( )
    {
        $this->initOptions([
            'message' => __('Boolian True')
        ]);
    }

    protected function validateImpl($value)
    {
        return true === $value;
    }
}
