<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.0.0
 */
namespace Nora\Module\Validation\Rule;
use Nora\Core\Options\OptionsAccess;

use function Nora\__;

class AllOf extends Base
{
    private $_validators;

    public function __call($name, $args)
    {
        $this->addValidator(self::build($name, $args));
        return $this;
    }

    static public function build($name, $spec)
    {
        $class = __namespace__.'\\'.ucfirst($name);
        return $class::build($spec);
    }

    protected function addValidator ($validator)
    {
        $this->_validators[] = $validator;
    }

    protected function getValidators( )
    {
        foreach($this->_validators as $v)
        {
            yield $v;
        }
    }


    protected function validateImpl($value)
    {
        $hasError = false;
        $this->_invalid_rules = [];
        foreach($this->_validators as $v)
        {
            if (!$v->validate($value))
            {
                $this->_invalid_rules[] = $v;
                $hasError = true;
            }
        }

        return !$hasError;
    }

    public function getMessage( )
    {
        foreach($this->_invalid_rules as $v)
        {
            return $v->getMessage();
        }
    }
}
