<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.0.0
 */
namespace Nora\Module\Validation\Rule;
use Nora\Core\Options\OptionsAccess;

use function Nora\__;

class Offset extends Base
{
    public function __construct($selector, $validator)
    {
        $this->_selector = $selector;
        $this->_validator = $validator;
        $this->initRuleImpl();
    }

    protected function validateImpl($value)
    {
        if(false === (is_array($value) || $value instanceof \ArrayAccess))
        {
            return false;
        }

        return $this->_validator->validate(
            isset($value[$this->_selector]) ?
            $value[$this->_selector]:
            null
        );
    }

    static public function build($spec)
    {
        $class = get_called_class();
        $key = $spec[0];
        $related_validator = $spec[1];
        $options  = isset($spec[2]) ? $spec[2]: [];

        $validator = new $class($key, $related_validator);
        $validator->setOption($options);
        return $validator;
    }

}
