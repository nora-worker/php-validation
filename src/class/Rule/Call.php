<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.0.0
 */
namespace Nora\Module\Validation\Rule;
use Nora\Core\Options\OptionsAccess;

use function Nora\__;

class Call extends Base
{
    protected function initRuleImpl( )
    {
        $this->initOptions([
            'message' => __('Boolian True'),
            'callback' => '',
            'validator' => ''
        ]);
    }

    protected function validateImpl($value)
    {
        return $this->getOption('validator')
            ->validate(
                call_user_func($this->getOption('callback'), $value)
            );
    }

    static public function build($spec)
    {
        $class = get_called_class();
        $validator = new $class();
        $cb = $spec[0];
        $v = $spec[1];
        $options = isset($spec[2]) ? $spec[2]: [];
        $validator->setOption($options + [
            'callback' => $cb,
            'validator' => $v
        ]);
        return $validator;
    }
}
