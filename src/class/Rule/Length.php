<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.0.0
 */
namespace Nora\Module\Validation\Rule;
use Nora\Core\Options\OptionsAccess;

use function Nora\__;

class Length extends Base
{
    protected function initRuleImpl( )
    {
        $this->initOptions([
            'message' => __('{{min}}から{{max}}の間で入力してください'),
            'min' => 0,
            'max' => 999
        ]);
    }

    protected function validateImpl($value)
    {
        $min = $this->getOption('min');
        $max = $this->getOption('max');
        $len = strlen($value);

        return $len <= $max && $len >= $min;
    }

    static public function build($spec)
    {
        $class = get_called_class();
        $validator = new $class();
        $min = $spec[0];
        $max = $spec[1];
        $options = isset($spec[2]) ? $spec[2]: [];
        $validator->setOption($options + [
            'min' => $min,
            'max' => $max
        ]);
        return $validator;
    }
}
