<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.0.0
 */
namespace Nora\Module\Validation;


class Validator extends Rule\AllOf
{
    static public function __callStatic($name, $args)
    {
        $v = new Validator();
        return call_user_func_array([$v, $name], $args);
    }


    public function explain( )
    {
        $explanation = [];
        foreach($this->getValidators() as $v)
        {
            $explanation[] = $v->getMessage();
        }
        return $explanation;
    }
}
