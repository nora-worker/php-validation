<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.0.0
 */
namespace Nora\Module\Validation;

use Nora;
use Nora\Module\Validation\Validator as v;

/**
 * テスト
 */
class ModuleTest extends \PHPUnit_Framework_TestCase
{
    public function testNotNull ( )
    {
        $v = Validator::notnull([
            'message' => 'Nullは許可されていません'
        ]);

        $this->assertFalse($v->validate(null));

        $this->assertEquals(
            'Nullは許可されていません', $v->getMessage()
        );

        $v = v::offset('name', v::string(), [
            'message' => 'ユーザ名が不正です'
        ]);


        $this->assertTrue($v->validate(['name' => 'hajime']));
        $this->assertFalse($v->validate(['name' => 1234]));

        $this->assertEquals(
            'ユーザ名が不正です', $v->getMessage($v->getMessage())
        );

        $v = v::string()->int()->object()->length(5,10)->notnull();
        $this->assertFalse($v->validate(111));

        var_dump($v->getMessage());
        var_dump($v->explain());

        $passwd = '1234';
        $v = v::call(function($input) use ($passwd) {
            return $input === $passwd;
        }, v::true(), [
            'message' => 'パスワード不一致'
        ]);

        vaR_dump($v->validate(1223));
        vaR_dump($v->validate('1234'));
    }
}
